import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';

// const firebaseConfig = {
//     apiKey: "AIzaSyAaLhtQ-B698GWyLNihGVRaNWBOKtBH8wU",
//     authDomain: "evernoteclone-7682f.firebaseapp.com",
//     projectId: "evernoteclone-7682f",
//     storageBucket: "evernoteclone-7682f.appspot.com",
//     messagingSenderId: "332984082327",
//     appId: "1:332984082327:web:ae2776c3a56f4d98816ed2"
// };

const firebaseConfig = {
    apiKey: "AIzaSyDK1XWcBP5d0p639uFq9Jvyy-8QnDQPjrs",
    authDomain: "chat-toan.firebaseapp.com",
    projectId: "chat-toan",
    storageBucket: "chat-toan.appspot.com",
    messagingSenderId: "998851750856",
    appId: "1:998851750856:web:a7d2de150a98c8e9936efd",
    measurementId: "G-WZ9QGCMMBC"
  };

export const app = initializeApp(firebaseConfig);
export const database = getFirestore(app);