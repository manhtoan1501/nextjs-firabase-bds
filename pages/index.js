import Head from 'next/head'
import HeaderHome from '../components/HeaderHome';
import Advertise from '../components/Advertise';
import GeographicalLocation from '../components/GeographicalLocation';
import ViewFull from '../components/ViewFull';
import ViewHoaBinh from '../components/ViewHoaBinh';
import Utilities from '../components/Utilities';
import Image360 from '../components/Image360';
import LuxHome from '../components/LuxHome';
import ProjectOverview from '../components/ProjectOverview';
import Introduce from '../components/Introduce';
import ReasonCasaHot from '../components/ReasonCasaHot';
import Subdivision from '../components/Subdivision';
import FixedComponent from '../components/FixedComponent';
import Footer from '../components/Footer';

function Home() {

  return (
    <div className='home-root'>
      <Head>
        <title>Casa Del Rio Hòa Bình</title>
        <meta name="description" content="Casa Del Rio Hòa Bình" />
        <link href="https://fonts.googleapis.com/css2?family=Inter" rel="stylesheet" />
        <link rel="shortcut icon" href="https://casa-delrio.vn/wp-content/uploads/2022/03/favicon-casa-del-rio-hoa-binh-50x50-1.png" type="image/x-icon"></link>
      </Head>

      <main className='main-roots' id="toTop" >
        <HeaderHome />
        <Advertise />
        <GeographicalLocation />
        <ViewFull />
        <ViewHoaBinh />
        <Utilities />
        <Image360 />
        <LuxHome />
        <ProjectOverview />
        <Introduce />
        <ReasonCasaHot />
        <Subdivision />
        <FixedComponent />
      </main>
      <Footer />
    </div>
  )
}

export default Home;
