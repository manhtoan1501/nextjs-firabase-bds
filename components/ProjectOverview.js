import { Grid } from '@material-ui/core';


const ProjectOverview = () => {
  const overviewProject = [
    {
      title: 'Chủ đầu tư',
      name: 'Công ty TNHH khu đô thị mới Trung Minh'
    },
    {
      title: 'Vị trí dự án',
      name: 'xã Trung Minh, thành phố Hòa Bình, tỉnh Hòa Bình'
    },
    {
      title: 'Đơn vị phát triển dự án',
      name: 'Cty CP Quản lý đầu tư và phát triển dự án Think Tank Việt Nam'
    },
    {
      title: 'Quy mô dự án',
      name: '142,41 ha'
    },
    {
      title: 'Mật độ xây dựng',
      name: '42%'
    },
    {
      title: 'Loại hình phát triển',
      name: 'Shophouse, biệt thự song lập, đơn lập'
    },

    {
      title: 'Số lượng sản phẩm',
      name: 'hơn 2000 sản phẩm'
    },
    {
      title: 'Diện tích sản phẩm',
      name: '89,1 – hơn 1215,9m2'
    },
    {
      title: 'Hình thức sở hữu',
      name: 'Sổ đỏ lâu dài'
    },
  ];

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12} sm={6}>
        <h1 className='title-base'>
          TỔNG QUAN DỰ ÁN
        </h1>
        {overviewProject.map((element, index) => (
          <Grid container justifyContent='space-between' key={index}>
            <Grid item xs={6}>
              <p className='content-base'>
                <strong>
                  {element.title}
                </strong>
              </p>
            </Grid>
            <Grid item xs={6}>
              <p className='content-base'>
                {element.name}
              </p>
            </Grid>
          </Grid>
        ))}
      </Grid>
      <Grid item xs={12} sm={6}>
        <img
          src={'https://casa-delrio.vn/wp-content/uploads/2022/04/phoi-canh-du-an-casa-del-rio-hoa-binh-2-800x420.jpg'}
          className='image-overview-project'
        />
      </Grid>
    </Grid>
  );
}

export default ProjectOverview;
