import { Grid } from '@material-ui/core';

const Introduce = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          VIDEO GIỚI THIỆU DỰ ÁN
        </h1>
      </Grid>
      <Grid item xs={12} md={6}>
        <iframe
          title="Video giới thiệu dự án Casa Del Rio"
          src="https://www.youtube.com/embed/KazkZqW-LOs?autoplay=0&amp;enablejsapi=1&amp;wmode=opaque"
          allowFullScreen=""
          allow="autoplay; fullscreen"
          name="fitvid0"
          className='elementor-video-introduce'
          width="100%"
          height="300px"
          id="player_1">
        </iframe>
      </Grid>
      <Grid item xs={12} md={6}>
        <iframe
          title="Video giới thiệu dự án Casa Del Rio"
          src="https://casa-delrio.com/wp-content/uploads/2021/12/Casa-Del-Rio-Dia-Linh-Sinh-Phu-Quy.mp4"
          allowFullScreen=""
          className='elementor-video-introduce'
          allow="autoplay; fullscreen"
          name="fitvid0"
          width="100%"
          height="300px"
          id="player_1">
        </iframe>
      </Grid>
    </Grid>
  );
}

export default Introduce;
