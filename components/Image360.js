import { Grid } from '@material-ui/core';

const Image360 = () => {

  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          ẢNH 360 ĐỘ
        </h1>
        <iframe
          src="https://momento360.com/e/u/44be933cf1fb41039a0cc0b0a570d413"
          name="fancybox-frame1598541477185"
          width="100%"
          height="600px"
          scrolling="auto"
          allowFullScreen="allowFullScreen">
        </iframe>
      </Grid>
    </Grid>
  );
}

export default Image360;
