import { Grid } from '@material-ui/core';


const Subdivision = () => {

  const subdivisions = [
    {
      title: 'PHÂN KHU ROMA',
      numberHouse: 406,
      area: '32.83',
      image: 'https://w.ladicdn.com/s850x650/5ea845b95da1a2557f302daa/phan-khu-a-20220627023150.jpg',
      content: 'PHÂN KHU BERLINPhân khu Roma nổi bật với công viên Ngọc Trai, nơi sở hữu những tiện ích cao cấp như: Bể bơi vô cực, đường đi bộ, khu tập gym ngoài trời....',
    },
    {
      title: 'PHÂN KHU BERLIN',
      numberHouse: 837,
      area: '58.87',
      image: 'https://w.ladicdn.com/s850x700/5ea845b95da1a2557f302daa/phan-khu-b-20220627023150.jpg',
      content: 'Tại phân khu này không chỉ sở hữu các căn nhà, biệt thự hiện đại, đầy đủ tiện ích mà còn được thiết kế không gian mở rất phù hợp và tiềm năng trong việc kinh doanh sinh lời',
    },
    {
      title: 'PHÂN KHU PARIS',
      numberHouse: 764,
      area: '50.71',
      image: 'https://w.ladicdn.com/s850x650/5ea845b95da1a2557f302daa/phan-khu-c-20220627023150.jpg',
      content: 'Thiết kế tại phần khu Paris thiên về phong cách mở, tạo ra không gian sôi động sầm uất cho toàn bộ đô thị. Không gian sống chan hòa với thiên nhiên',
    }
  ];
  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          CÁC PHÂN KHU CỦA CASA DEL RIO
        </h1>
      </Grid>
      {subdivisions.map((element, index) => (
        <Grid item xs={12} md={4} key={index}>
          <div className='subdivision-item-root'>
            <img
              src={element.image}
              className='image-subdivision'
            />
            <strong className='title-subdivision'>
              {element.title}
            </strong>
            <p className='content-subdivision'>
              Số căn: {element.numberHouse} căn
            </p>
            <p className='content-subdivision'>
              Diện tích: {element.numberHouse} ha
            </p>
            <p className='content-subdivision'>
              {element.content}
            </p>
          </div>
        </Grid>
      ))}
    </Grid>
  );
}

export default Subdivision;
