import { Grid } from '@material-ui/core';


const ReasonCasaHot = () => {

  const reasonCasaHot = [
    {
      title: 'Trong lành',
      content: 'Nhà ở ven sông thường có không khí rất mát mẻ, trong lành. Qua đó giúp bạn tăng chất lượng sống, tăng tuổi thọ, gia đình gắn kết hạnh phúc hơn.',
    },
    {
      title: 'Thoáng đãng',
      content: 'Với tầm view đẹp, rộng mở, từ ngôi nhà, bạn có thể phóng tầm mắt ngắm khung cảnh thơ mộng xung quanh. Qua đó giúp bạn thư giãn hơn sau những giờ làm việc căng thẳng.',
    },
    {
      title: 'Hợp phong thủy',
      content: '“Tựa sơn, hướng thủy” luôn là tiêu chí hàng đầu khi lựa chọn các vùng đất để định cư của người xưa. Các vùng đất như vậy thường rất an toàn và nhiều vượng khí.',
    },
    {
      title: 'Hiếm có',
      content: 'Với khu vực miền Bắc rất hiếm các dự án bất động sản nằm liền kề ven sông. Đặc biệt với dòng sông lớn và hùng vĩ như sông Đà thì chỉ có duy nhất Casa Del Rio.',
    }
  ];
  return (
    <Grid container className='block-location max-width-base' spacing={2}>
      <Grid item xs={12}>
        <h1 className='title-base'>
          LÝ DO BẤT ĐỘNG SẢN VEN SÔNG ĐƯỢC SĂN ĐÓN
        </h1>
      </Grid>
      {reasonCasaHot.map((element, index) => (
        <Grid key={index} item xs={12} sm={6} md={3}>
          <div className='item-reason-casa'>
            <p className='title-reason'>
              {element.title}
            </p>
            <p className='content-reason'>
              {element.content}
            </p>
          </div>
        </Grid>
      ))}
    </Grid>
  );
}

export default ReasonCasaHot;
