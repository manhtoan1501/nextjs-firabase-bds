import React from 'react';
import { ArrowUpward } from '@material-ui/icons';
import Form from './Form';
import {
  Button,
  IconButton,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';

const FixedComponent = () => {

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className='fixed-root'>
      <div className='fixed-item'>
        <a className='phone-admin fixed-contact' rel='nofollow noopener' href='tel:+84979194717' target='_parent'>
          0979 194 717
        </a>
      </div>
      <div className='fixed-item'>
        <Button
          variant='contained'
          color='default'
          style={{
            backgroundColor: '#fff',
            color: '#4caf50'
          }}
          onClick={handleClickOpen}
        >
          Nhận bảng giá
        </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          style={{ padding: 32 }}
        >
          <DialogTitle style={{ color: '#4caf50', fontWeight: 700, fontSize: 20 }}>
            Đăng kí nhận bảng giá tốt nhất
          </DialogTitle>
          <DialogContent style={{ padding: 32 }}>
            <Form handleClose={handleClose} />
          </DialogContent>
        </Dialog>
      </div>
      <div className='fixed-item'>
        <a href='https://zalo.me/0979194717' target='_blank' className='zalo-admin ' data-replace-href='https://zalo.me/0979194717'>
          <img src={'https://banghevanphonghcm.com/wp-content/uploads/2022/06/icon-zalo.png'} />
        </a>
      </div>
     
      {/* <a href='#toTop' className='scroll-to-top'>
        <ArrowUpward style={{ fontSize: 40, color: '#4caf50' }} />
      </a> */}
    </div>
  );
}

export default FixedComponent;
